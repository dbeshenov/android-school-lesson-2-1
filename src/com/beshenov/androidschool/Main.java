package com.beshenov.androidschool;

import java.util.Locale;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        new Thread(new Step("Left")).start();
        new Thread(new Step("Right")).start();
    }

    private static class Step implements Runnable {

        private final String mName;

        private Step(String name) {
            mName = name;
        }

        @Override
        public void run() {
            // так как по условию задания можно менять только метод run(),
            // в качестве монитора используется статический объект из стандартной библиотеки,
            // хотя лучше было бы определить свой монитор
            synchronized (Locale.ENGLISH) {
                for (int i = 0; i < 100; ++i) {
                    Locale.ENGLISH.notifyAll();
                    System.out.println(mName);
                    try {
                        Locale.ENGLISH.wait();
                    } catch (InterruptedException e){
                    }
                }
                Locale.ENGLISH.notifyAll();
            }
        }
    }
}
